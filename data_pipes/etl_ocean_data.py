import datetime, json
import logging
from airflow.models import Variable

from python_code.tool_belt.AwsSledgeHammer import OpenAws
from python_code.tool_belt.DbPipeWrench import OpenDb


class TransLoadSchedulesData:
    def __init__(self, vt_bucket, filename):
        self.bucket = vt_bucket
        self.filename = filename
        self.main()

    def getShipSchedules(self):
        self.aws.s3Init(self.bucket)
        filename = self.filename
        filedata = self.aws.getFileFromS3Bucket(filename, decompress=True)
        return  json.loads(filedata)


    def loadScheduleArrays(self, schedule_results):
        file_name = self.filename
        if file_name != None:
            temp_time = file_name.split('_')[-1]
            temp_time = temp_time.split('.')[0]
            time = datetime.datetime(int(temp_time[:4]), int(temp_time[4:6]), int(temp_time[6:8]), int(temp_time[8:10]), int(temp_time[10:12]))
            
        else:
            time = datetime.datetime.utcnow()

        for result in schedule_results:
            schedules = result.get("schedules")
            for schedule in schedules:
                self.portLocode.append(schedule.get('portLocode'))
                self.portName.append(schedule.get('portName'))
                self.portCountry.append(schedule.get('portCountry'))
                self.lat.append(schedule.get('lat'))
                self.lon.append(schedule.get('lon'))
                self.timeStart.append(schedule.get('timeStart'))
                self.timeEnd.append(schedule.get('timeEnd'))
                self.imo.append(result['shipInfo'].get('imo'))
                self.timePulled.append(time)
    def verifyShipInShips(self):

        query = "SELECT imo FROM base.ships;"

        ships = self.db.executeSelectQuery(query)

        # O(n) option         
        imos = {}
        for ship in ships:
            imos[int(ship[0])] = True

        # O(n^2) option 
        # imos=[int(ship[0]) for ship in ships]

        recently_added = {}
        for i in range(len(self.imo)):
            current_imo = int(self.imo[i])

            
            if (imos.get(current_imo) != True) and (recently_added.get(current_imo) != True  ): # O(n) option

                recently_added[current_imo] = True # O(n) option
                # recently_added.append(current_imo) # O(n^2) option 
            
                query =  "INSERT INTO base.ships VALUES ('{}','{}','{}','NULL','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(    
                                                current_imo,
                                                self.ships[current_imo].get('name'),
                                                self.ships[current_imo].get('mmsi') if self.ships[current_imo].get('mmsi') != None else 0,
                                                0,
                                                0,
                                                0,
                                                0,
                                                self.ships[current_imo].get('length'),
                                                self.ships[current_imo].get('width'),
                                                0,
                                                self.ships[current_imo].get('aisShiptype'),
                                                "A")
    
                self.db.execute(query)
    def verifyPortInPorts(self):
        query = "SELECT locode, portName, country FROM base.ports;"

        ports = self.db.executeSelectQuery(query)
        locodes, port_names, countries = [],[],[]
        for port in ports:
            locodes.append(port[0]) 
            port_names.append(port[1])
            countries.append(port[2])

        recently_added = []
        for i in range(len(self.portLocode)):
            if (self.portLocode[i] not in locodes) and (self.portLocode[i] not in recently_added ):
                recently_added.append(self.portLocode[i])
                query =  "INSERT INTO base.ports VALUES ('{}','{}','{}',{})".format(    
                                                self.portLocode[i],
                                                self.portName[i].replace("'", "''"),
                                                self.portCountry[i].replace("'", "''"),
                                                0)
                self.db.execute(query)

    def refreshDbData(self):
        start_index = self.db.getLastKeyFromDBTable('stop_id', 'base.schedules') + 1
        load_command = """INSERT INTO base.schedules VALUES %s"""
        args = [(
                start_index + i,
                self.portLocode[i], 
                self.lat[i],
                self.lon[i],
                self.timeStart[i],
                self.timeEnd[i],
                self.imo[i],
                self.timePulled[i]
                ) for i in range(len(self.portLocode))]
        self.db.bulkLoad([load_command, args])

    def getSchedulesFromDb(self):
        query = """SELECT DISTINCT timepulled FROM base.schedules"""
        query_result = self.db.executeSelectQuery(query)
        query_result = [arg[0] for arg in query_result]
        return query_result

    def main(self):


        TARGET_FOLDER = "raw_schedules"
        END = datetime.datetime.utcnow()
        START = END - datetime.timedelta(minutes=45)

        self.TARGET_FOLDER = TARGET_FOLDER
        self.END = END
        self.START = START


        ##### MAIN() #####


        ### Load Data to AWS and DB ###
        # Connect to AWS Boto3

        aws = OpenAws()
        self.aws = aws

        ENV =  Variable.get('ENV')
        args = aws.getVtConnectInfo(environment=ENV)


        db = OpenDb(args)
        self.db = db

        # get ship schedules
        data = self.getShipSchedules()

        if data == None:
            quit()

        db_data = self.getSchedulesFromDb()

        self.portLocode, self.lat, self.lon, self.timeStart, self.timeEnd, self.imo, self.timePulled, self.portName, self.portCountry = [],[],[],[],[],[],[],[],[]

        self.loadScheduleArrays(data)

        self.verifyShipInShips()
        self.verifyPortInPorts()

        if self.timePulled[0] in db_data:
            logging.debug("Schedule from ",self.timePulled[0], "already exists in the database")
            quit()

        self.refreshDbData()

        db.close()
