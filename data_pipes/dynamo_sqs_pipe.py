from python_code.tool_belt.AwsSledgeHammer import OpenAws


from boto3.dynamodb.conditions import Attr
from datetime import datetime
import json
import logging


class DailyPlanetSqsLoader:
        
    """ This class loads the Daily Planet SQS feed"""
    def __init__(self) -> None:
        pass

    def getNonEnrichedFeeds(self, aws, TABLE_NAME):
        expression = Attr("enriched").eq(False)
        incoming_feeds = aws.scanDynamoTable(TABLE_NAME, expression)
        return incoming_feeds

    def getSqsTableData(self, aws, TABLE_NAME):
        sqs_table_dict = {}
        sqs_table_data = aws.scanDynamoTable(TABLE_NAME, None)
        if sqs_table_data == None:
            return sqs_table_dict
        else:
            for item in sqs_table_data:
                sqs_table_dict[item["url"]] = True
        return sqs_table_dict
        
    def loadSqs(self, aws, QUEUE_NAME, MESSAGES):
        for message in MESSAGES:
            response = aws.sendSqsMessage(QUEUE_NAME, json.dumps(message))
            print(response)

    def updateSqsTable(self, aws, SQS_TABLE_NAME, MESSAGES):
        now = datetime.utcnow()
        for message in MESSAGES:
            data = {"url":message["article_url"], "date":now.strftime("%Y%m%d%H%M%S")} #YYYYMMDDHHMMSS
            response = aws.putItemDynamo(SQS_TABLE_NAME, data)
            print(response)


    def loadSqsFeed(self):
        sqs_feeds = []
        SQS_NAME = "sanitized"
        ARTICLE_TABLE_NAME = "sanitized"
        SQS_TABLE_NAME = "sanitized"

        
        aws = OpenAws()

        # get non enriched feeds from db
        non_enriched_feeds = self.getNonEnrichedFeeds(aws, ARTICLE_TABLE_NAME) 
        # get list of feeds already submitted to sqs
        sqs_table_data = self.getSqsTableData(aws, SQS_TABLE_NAME)

        # compare new feeds to feeds previously submitted to SQS and remove dupes
        if len(non_enriched_feeds) > 0:
            for feed in non_enriched_feeds:
                if sqs_table_data.get(feed["article_url"]) == None:
                    sqs_feeds.append(feed)


        logging.error(sqs_feeds)

        #load new data to sqs feed 
        self.loadSqs(aws, SQS_NAME, sqs_feeds)

        #update sqs table w/ latest data
        self.updateSqsTable(aws, SQS_TABLE_NAME, sqs_feeds)

        return sqs_feeds
