import logging
import json
import gzip
import csv
import boto3
import io
import time
import datetime
import urllib

def handler(event, context):
    # TODO implement
    def awsConnect():
        my_session = boto3.Session()
    
        return my_session
    
        
    def writeFilesToS3(s3, key, bucketname = "sanitized bucket name"):
        filepath = "transformed/eta-data/" + key.split("/",1)[1] + ".csv.gz"
        write_file = io.StringIO()
        bytes_file = io.BytesIO()
        labels = ["version", "sanitized", "inputs", "data", "sanitized", "event_date", "org_id", "customer_id", "sanitized", "sanitized", "sanitized", "geom", "data_owner", "data_owner_id"]
        

        writer = csv.writer(write_file)
        writer.writerow(labels)
        for i in range(len(geoms)):
            temp_data = [VERSION, sanitized[i], sanitized[i], sanitized[i], sanitized[i], sanitized[i], sanitized[i], sanitized[i], sanitized[i], sanitized[i], sanitized[i], sanitized[i], data_owner[i], data_owner_id[i] ]
            writer.writerow(temp_data)
    
        # write .csv using gzip/csv
        with gzip.GzipFile(fileobj = bytes_file, mode='wb') as f:
            f.write(write_file.getvalue().encode(encoding='utf-8'))
            
        bytes_file.seek(0)
        s3.upload_fileobj(bytes_file, bucketname, filepath, { 'ContentEncoding': 'gzip'})
        
    def transformData(input_data):    
        # split input log string to array. 
        json_array = [] 
        s = "" 
        for i in range(len(input_data)):
            s += input_data[i]
            if i > 5:
                if (input_data[i] == '}' and input_data[i-1] == ']' and input_data[i-2] == '}' and input_data[i-3] == '"' and input_data[i-4] == '}' and input_data[i-5] == '}'):
                    json_array.append(json.loads(s))
                    s = ""
    
        #Loop through each input from the raw log stream
        for i in range(len(json_array)):

            #Loop through the each shipment per input 
            for j in range(len(json_array[i]['sanitized'])):
                message_dict = json_array[i]["sanitized"][j]["message"]
                message = json.loads(message_dict)

    
                if message.get('sanitized') and message.get('sanitized'):
                   
                    # SANITIZED 
                    nests = message['sanitized'].get("sanitized")        
                    nests.pop("sanitized", None)             # remove data included from previous runs
                    sanitized.append(nests)                   # add payload data to array
    
                    # SANITIZED DATA
                    nests = message['sanitized'].get("inputs")
                    sanitized.append(nests)
    
                    payload_data = message["sanitized"].pop("payload", None)
                    message["sanitized"].pop("inputs", None)
                    
                    # SANITIZED 
                    sanitized.append(message["sanitized"].pop("sanitized", None))
    
                    # SANITIZED 
                    sanitized.append(message["sanitized"].pop("created_date", None))
                    
                    # DATA OWNERSHIP
                    if (payload_data["org"].get("parent")):
                        nests = payload_data["org"].get("parent")
                        data_owner.append( nests.get("name"))
                        data_owner_id.append(nests.get("id"))

                    else:
                        nests = payload_data.get("org")
                        data_owner.append( nests.get("name"))
                        data_owner_id.append(nests.get("id"))
    
                    
                    nests = message.get("sanitized")
                    
                    data.append(nests)
                    
                    

                    
                    # SANITIZED
                    nests = message['sanitized'].get("sanitized")
                    sanitized_0.append(nests)
                    # SANITIZED
                    nests = message['sanitized'].get("sanitized")
                    sanitized_1.append(nests)
                    # SANITIZED
                    nests = message['sanitized'].get("sanitized")
                    sanitized_2.append(nests)
                    # SANITIZED
                    nests = message['sanitized'].get("sanitized")
                    sanitized_3.append(nests)
                    # SANITIZED
                    nests = message['sanitized'].get("sanitized")
                    sanitized_4.append(nests)
                                
                    geoms.append(message["sanitized"].get("geom"))
            
    
    ### Variables ###
    tic_global = time.perf_counter()
    
    ALL_FILES = True
    VERSION = 11
    
                            
    non_loaded_files, error_log = [], []
    
    ### Main() ###
    my_session = awsConnect()
    s3 = my_session.client('s3')

    
    bucketname = event["Records"][0]['s3']['bucket']['name']
    file_to_read = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding = 'utf-8')
    if file_to_read.split("/",1)[0] != "backup":
        return None
    
    #Create a file object using the bucket and object key. 
    try:    
        #### Download File from S3 and decompress ###   
        #Create a file object using the bucket and object key. 
        fileinput = s3.get_object(
            Bucket=bucketname,
            Key=file_to_read
            ) 
        # # open the file object and read it into the variable filedata. 
        filedata = fileinput['Body'].read()
        fileobj = io.BytesIO(filedata)
    
        # # file data will be a binary stream.  We have to decode it 
        with gzip.open(fileobj, 'rt') as f:
            input_data = f.read()

        if str(input_data).find('"messageType":"DATA_MESSAGE"') != -1:

            output_data = input_data
            transformData(input_data)
            writeFilesToS3(s3, file_to_read)
    
    except Exception as e:
        non_loaded_files.append(file_to_read)
        error_log.append("reason for exception:\n{}".format( e))
        logging.error("reason for exception:\n{}".format(e))

    
    
    tNow = datetime.datetime.utcnow()
    loading_failure_path = "transformed/failed_to_load/{}/{}/{}/{}/loading_failure_log{}.txt.gz".format(tNow.strftime("%Y"), tNow.strftime("%m"),tNow.strftime("%d"),tNow.strftime("%H"), tNow.strftime("%Y%m%d%H%M%S"))
    error_log_path = "transformed/error_log/{}/{}/{}/{}/error_log{}.txt.gz".format(tNow.strftime("%Y"), tNow.strftime("%m"),tNow.strftime("%d"),tNow.strftime("%H"), tNow.strftime("%Y%m%d%H%M%S"))

        
    write_file = io.StringIO("{}".format(', '.join(non_loaded_files)))
    bytes_file = io.BytesIO()
    with gzip.GzipFile(fileobj = bytes_file, mode='wb') as f:
            f.write(write_file.getvalue().encode(encoding='utf-8'))
    bytes_file.seek(0)
    s3.upload_fileobj(bytes_file, bucketname, loading_failure_path, { 'ContentEncoding': 'gzip'})
    
    
    
    write_file = io.StringIO("{}".format(', '.join(error_log)))
    bytes_file = io.BytesIO()
    with gzip.GzipFile(fileobj = bytes_file, mode='wb') as f:
            f.write(write_file.getvalue().encode(encoding='utf-8'))
    bytes_file.seek(0)
    s3.upload_fileobj(bytes_file, bucketname, error_log_path, { 'ContentEncoding': 'gzip'})        
    
    toc_global = time.perf_counter()
    logging.info(f"END time {toc_global - tic_global:0.4f} seconds")
    
