[[ "$TRACE" ]] && set -x

export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
export CI_APPLICATION_TAG=$CI_COMMIT_SHA
export CI_CONTAINER_NAME=ci_job_build_${CI_JOB_ID}
# Extract "MAJOR.MINOR" from CI_SERVER_VERSION and generate "MAJOR-MINOR-stable" for Security Products
export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
echo "$CI_REGISTRY_USER $CI_REGISTRY_PASSWORD $CI_REGISTRY"

function registry_login() {
  if [[ -n "$CI_REGISTRY_USER" ]]; then
    echo "Logging to GitLab Container Registry with CI credentials..."
    docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    echo ""
  fi
}

## LEGACY
function build() {
  registry_login

  if [[ -f Dockerfile ]]; then
    echo "Building Dockerfile-based application..."
    echo "Repo: $CI_APPLICATION_REPOSITORY"
    echo "Variables $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" 

    # Airflow build
    echo "Building Airflow Image"
   docker build \
     -t "$CI_APPLICATION_REPOSITORY/$BC_ENV:$CI_APPLICATION_TAG" \
     -t "$CI_APPLICATION_REPOSITORY/$BC_ENV:latest" .



  fi

  echo "Pushing to GitLab Container Registry..."
  echo "Thank you for playing!!!"

  docker push "$CI_APPLICATION_REPOSITORY/$BC_ENV:$CI_APPLICATION_TAG"
  docker push "$CI_APPLICATION_REPOSITORY/$BC_ENV:latest"
  echo ""
}

function install_dependencies() {

    # Add nodejs and npm to main system
    apk add --update nodejs npm python3 py3-pip make g++ docker

    # npm init
    npm install


    # Install serverless cli
    npm config set prefix /usr/local
    npm install -g serverless
    npm install --save-dev serverless-wsgi serverless-python-requirements

  }

function deploy_serverless_project() {
    find . -type d -name __pycache__ -exec rm -r {} \+
    
    cd $FOLDER_PATH

    sls deploy -c serverless.yml --region $AWS_DEFAULT_REGION --stage $CI_ENVIRONMENT_SLUG --verbose
  }

function deploy_parser() {
    find . -type d -name __pycache__ -exec rm -r {} \+
    
    sls deploy -c parser-serverless.yml --region $AWS_DEFAULT_REGION --stage $CI_ENVIRONMENT_SLUG --verbose
  }

function var_test() {
    echo "The variable is: $TEST_VAR_JOB"
    echo "The environment is: $DP_ENV"
    echo "The environment slug is: $CI_ENVIRONMENT_SLUG"
    echo "The environment name is: $CI_ENVIRONMENT_NAME"
  }
