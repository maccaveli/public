import simplejson as sjson
import json
from decimal import Decimal
from flask import Flask, jsonify

from tool_belt.AwsSledgeHammer import OpenAws



def awsConnect():
    aws = OpenAws()
    return aws

global aws

aws = awsConnect()

app = Flask(__name__)

@app.route('/', methods=['GET'])
def welcome():
    return "Hello World!"


@app.route('/test/', methods=['GET'])
def test():
    message = jsonify([{'greeting' : 'hello from flask'},{'greeting' : 'lorem ipsom'}])
    message.headers.add('Access-Control-Allow-Origin', '*')
    return message


### uncomment to run in local env ###
# if __name__ == '__main__':
#     app.run(host="0.0.0.0", port=8080)

    
