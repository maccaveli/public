variable "common_tags" {
  type = map
  default = {
    "cost:Service" = "Sanitized"
  }
}
##################################################################################

variable "env" {}
data "aws_availability_zones" "available" {}
variable "target_s3_datalake" {} 
variable "target_s3_id" {}
variable "source_db_user" {} 
variable "source_db_password" {} 
variable "source_endpoint" {} 
variable "source_port" {} 
variable "source_db_name" {}
variable "source_endpoint_id" {}
variable "target_user" {} 
variable "target_pass" {} 
variable "target_db_name" {} 
variable "target_endopint" {} 
variable "target_port" {} 
variable "target_endpoint_id" {} 
variable "replication_server_name" {}
variable "replication_vpc" {}
variable "replication_sg" {}
variable "instance_type" {}
variable "pg_to_rs_migration_type" {}

# Get VPC Subnet IDs #
data "aws_subnets" "environment_vpc" {
  filter {
    name   = "vpc-id"
    values = [var.replication_vpc]
  }
}


# S3 Datalake #
resource "aws_s3_bucket" "target_datalake" {
  bucket = var.target_s3_datalake
}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket                  = aws_s3_bucket.target_datalake.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


# Replication Subnet #
resource "aws_dms_replication_subnet_group" "sanitized" {
  replication_subnet_group_description = "DMS replication for sanitized subnet group"
  replication_subnet_group_id          = "sanitized"
  subnet_ids                           = data.aws_subnets.environment_vpc.ids
}

# Replication Instance #
resource "aws_dms_replication_instance" "replication_instance" {
  apply_immediately            = true
  auto_minor_version_upgrade   = true
  multi_az                     = false
  publicly_accessible          = false
  engine_version               = "3.4.6"
  replication_instance_class   = var.instance_type
  replication_instance_id      = "sanitized"
  replication_subnet_group_id  = "${aws_dms_replication_subnet_group.sanitized.id}"
  vpc_security_group_ids       = "${var.replication_sg}"

  tags = {
    Name        = "${var.replication_server_name}-dms"
    environment = "${var.env}"
    created_by  = "terraform"
  }
}

# Source Endpoint #
resource "aws_dms_endpoint" "postgres_source" {
  database_name = "${var.source_db_name}"
  endpoint_id   = "${var.source_endpoint_id}"
  endpoint_type = "source"
  engine_name   = "postgres"
  password      = "${var.source_db_password}"
  port          = "${var.source_port}"
  server_name   = "${var.source_endpoint}"
  ssl_mode      = "none"
  username      = "${var.source_db_user}"

  tags = {
    Name        = "${var.source_endpoint_id}-dms"
    environment = "${var.env}"
    created_by  = "terraform"
  }
}

# Target Redshift Endpoint #
resource "aws_dms_endpoint" "redshift_target" {
  database_name = "${var.target_db_name}"
  endpoint_id   = "${var.target_endpoint_id}-dms"
  endpoint_type = "target"
  engine_name   = "redshift"
  password      = "${var.target_pass}"
  port          = "${var.target_port}"
  server_name   = "${var.target_endopint}"
  ssl_mode      = "none"
  username      = "${var.target_user}"

  tags = {
    Name        = "${var.target_endpoint_id}-target"
    environment = "${var.env}"
    created_by  = "terraform"
  }
}

# Target S3 Endpoint #
resource "aws_dms_endpoint" "target_s3_endpoint" {
  endpoint_id  = "${var.target_s3_id}"
  endpoint_type = "target"
  # extra_connection_attributes = "DataFormat=parquet;parquetVersion=PARQUET_2_0;"
  engine_name = "s3"
  s3_settings  {
    service_access_role_arn = aws_iam_role.s3-access-role.arn
    bucket_name= "${var.target_s3_datalake}"
    # compression_type= "GZIP"
  }

  tags = {
    Name        = "${var.target_s3_id}-dms"
    environment = "${var.env}"
    created_by  = "terraform"
  }
}

# IAM Role to Access S3 Datalake #
resource "aws_iam_role" "s3-access-role" {
  name = "s3-access-${var.target_s3_datalake}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "dms.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "s3-access-policy" {
  name        = "s3-access-${var.target_s3_datalake}"
  description = "S3 Bucket Access Policy for DMS"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${var.target_s3_datalake}*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${var.target_s3_datalake}*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.s3-access-role.name
  policy_arn = aws_iam_policy.s3-access-policy.arn
}

# Migration Task Postgres to Redshift #
resource "aws_dms_replication_task" "pg-to-rs" {
  migration_type            = "${var.pg_to_rs_migration_type}"
  replication_instance_arn  = aws_dms_replication_instance.replication_instance.replication_instance_arn
  replication_task_id       = "pg-to-rs-task-tf"
  source_endpoint_arn       = aws_dms_endpoint.postgres_source.endpoint_arn
  table_mappings            = "{\"rules\":[{\"rule-type\":\"selection\",\"rule-id\":\"1\",\"rule-name\":\"1\",\"object-locator\":{\"schema-name\":\"%\",\"table-name\":\"%\"},\"rule-action\":\"include\"}]}"
  target_endpoint_arn       = aws_dms_endpoint.redshift_target.endpoint_arn


  tags = {
    Name = "postgres-to-redshift-task"
  }

  lifecycle {
    ignore_changes = [
      replication_task_settings,
    ]
  }
}

# Migration Task Postgres to S3 #
resource "aws_dms_replication_task" "pg-to-s3" {
  migration_type            = "full-load-and-cdc"
  replication_instance_arn  = aws_dms_replication_instance.replication_instance.replication_instance_arn
  replication_task_id       = "pg-to-s3-task-tf"
  source_endpoint_arn       = aws_dms_endpoint.postgres_source.endpoint_arn
  table_mappings            = "{\"rules\":[{\"rule-type\":\"selection\",\"rule-id\":\"1\",\"rule-name\":\"1\",\"object-locator\":{\"schema-name\":\"%\",\"table-name\":\"%\"},\"rule-action\":\"include\"}]}"
  target_endpoint_arn       = aws_dms_endpoint.target_s3_endpoint.endpoint_arn

  tags = {
    Name = "postgress-to-s3-migration-task"
  }

  lifecycle {
    ignore_changes = [
      replication_task_settings,
    ]
  }
}

### Output ###
output "s3_datalake" {
  value = var.target_s3_datalake
}
